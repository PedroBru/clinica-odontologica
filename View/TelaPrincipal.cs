﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace View
{
    public partial class TelaPrincipal : Form
    {
        public Form Tela { get; set; }

        static TelaPrincipal _obj;

        public static TelaPrincipal Instance
        {
            get
            {
                if (_obj == null)
                {
                    _obj = new TelaPrincipal();
                }
                return _obj;

            }
        }

        public Panel PnlContainer
        {
            get { return PainelEx; }
            set { PainelEx = value; }
        }

        public Button volta
        {
            get { return btnVoltar; }
            set { btnVoltar = value; }
        }

        public TelaPrincipal()
        {
            InitializeComponent();
        }
        //Metodo para fechar a aplicação
        private void BtnSair_Click_1(object sender, EventArgs e)
        {
            Close();
        }
        //Metodo para show em Tela
        private void TelaPrincipal_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            Tela.Show();
        }
        //Metodo para volta pra tela principal do sistema
        private void BtnVoltar_Click(object sender, EventArgs e)
        {
            PainelEx.Controls["PanelIni"].BringToFront();
            btnVoltar.Visible = true;
        }

        private void TelaPrincipal_Load_1(object sender, EventArgs e)
        {
            btnVoltar.Visible = true;
            _obj = this;

            PanelIni uc = new PanelIni();
            uc.Dock = DockStyle.Fill;
            PainelEx.Controls.Add(uc);

            // Metodo para bloquear os botões se o nivel de acessor for de atendente
            if (TelaLogin.acessor == "Atendente")
            {
                btnConta.Enabled = false;
            }
            if (TelaLogin.acessor == "Atendente")
            {
                btnExame.Enabled = false;
            }
            if (TelaLogin.acessor == "Atendente")
            {
                btnServico.Enabled = false;
            }
        }
    }
}