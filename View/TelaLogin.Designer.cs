﻿namespace View
{
    partial class TelaLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TelaLogin));
            this.panel1 = new System.Windows.Forms.Panel();
            this.picAvatarUsuario = new System.Windows.Forms.PictureBox();
            this.pnlTelaLogin = new System.Windows.Forms.Panel();
            this.grpEfetuarLogin = new System.Windows.Forms.GroupBox();
            this.cboAcessor = new System.Windows.Forms.ComboBox();
            this.lblnivelA = new MaterialSkin.Controls.MaterialLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picAvatarSenha = new System.Windows.Forms.PictureBox();
            this.picAvatarLogin = new System.Windows.Forms.PictureBox();
            this.txtSenha = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtLogin = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblsenha = new MaterialSkin.Controls.MaterialLabel();
            this.lbllogin = new MaterialSkin.Controls.MaterialLabel();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnEntrar = new System.Windows.Forms.Button();
            this.lblentrar = new MaterialSkin.Controls.MaterialLabel();
            this.lblsair = new MaterialSkin.Controls.MaterialLabel();
            this.panel = new System.Windows.Forms.Panel();
            this.errLogin = new System.Windows.Forms.ErrorProvider(this.components);
            this.errSenha = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAvatarUsuario)).BeginInit();
            this.pnlTelaLogin.SuspendLayout();
            this.grpEfetuarLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAvatarSenha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAvatarLogin)).BeginInit();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errSenha)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.YellowGreen;
            this.panel1.Controls.Add(this.picAvatarUsuario);
            this.panel1.Location = new System.Drawing.Point(44, 68);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 117);
            this.panel1.TabIndex = 12;
            // 
            // picAvatarUsuario
            // 
            this.picAvatarUsuario.Image = ((System.Drawing.Image)(resources.GetObject("picAvatarUsuario.Image")));
            this.picAvatarUsuario.Location = new System.Drawing.Point(231, 21);
            this.picAvatarUsuario.Name = "picAvatarUsuario";
            this.picAvatarUsuario.Size = new System.Drawing.Size(102, 71);
            this.picAvatarUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picAvatarUsuario.TabIndex = 6;
            this.picAvatarUsuario.TabStop = false;
            // 
            // pnlTelaLogin
            // 
            this.pnlTelaLogin.BackColor = System.Drawing.Color.YellowGreen;
            this.pnlTelaLogin.Controls.Add(this.grpEfetuarLogin);
            this.pnlTelaLogin.ForeColor = System.Drawing.Color.LightGoldenrodYellow;
            this.pnlTelaLogin.Location = new System.Drawing.Point(44, 191);
            this.pnlTelaLogin.Name = "pnlTelaLogin";
            this.pnlTelaLogin.Size = new System.Drawing.Size(581, 289);
            this.pnlTelaLogin.TabIndex = 13;
            // 
            // grpEfetuarLogin
            // 
            this.grpEfetuarLogin.BackColor = System.Drawing.Color.YellowGreen;
            this.grpEfetuarLogin.Controls.Add(this.cboAcessor);
            this.grpEfetuarLogin.Controls.Add(this.lblnivelA);
            this.grpEfetuarLogin.Controls.Add(this.pictureBox1);
            this.grpEfetuarLogin.Controls.Add(this.picAvatarSenha);
            this.grpEfetuarLogin.Controls.Add(this.picAvatarLogin);
            this.grpEfetuarLogin.Controls.Add(this.txtSenha);
            this.grpEfetuarLogin.Controls.Add(this.txtLogin);
            this.grpEfetuarLogin.Controls.Add(this.lblsenha);
            this.grpEfetuarLogin.Controls.Add(this.lbllogin);
            this.grpEfetuarLogin.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpEfetuarLogin.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.grpEfetuarLogin.Location = new System.Drawing.Point(29, 26);
            this.grpEfetuarLogin.Name = "grpEfetuarLogin";
            this.grpEfetuarLogin.Size = new System.Drawing.Size(523, 237);
            this.grpEfetuarLogin.TabIndex = 21;
            this.grpEfetuarLogin.TabStop = false;
            this.grpEfetuarLogin.Text = "Efetuar Login";
            // 
            // cboAcessor
            // 
            this.cboAcessor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAcessor.FormattingEnabled = true;
            this.cboAcessor.Items.AddRange(new object[] {
            "Medico",
            "Atendente"});
            this.cboAcessor.Location = new System.Drawing.Point(176, 144);
            this.cboAcessor.Name = "cboAcessor";
            this.cboAcessor.Size = new System.Drawing.Size(287, 30);
            this.cboAcessor.TabIndex = 32;
            // 
            // lblnivelA
            // 
            this.lblnivelA.AutoSize = true;
            this.lblnivelA.Depth = 0;
            this.lblnivelA.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblnivelA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblnivelA.Location = new System.Drawing.Point(67, 155);
            this.lblnivelA.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblnivelA.Name = "lblnivelA";
            this.lblnivelA.Size = new System.Drawing.Size(103, 19);
            this.lblnivelA.TabIndex = 31;
            this.lblnivelA.Text = "Nivel Acessor";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(29, 142);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            // 
            // picAvatarSenha
            // 
            this.picAvatarSenha.Image = ((System.Drawing.Image)(resources.GetObject("picAvatarSenha.Image")));
            this.picAvatarSenha.Location = new System.Drawing.Point(29, 88);
            this.picAvatarSenha.Name = "picAvatarSenha";
            this.picAvatarSenha.Size = new System.Drawing.Size(32, 32);
            this.picAvatarSenha.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picAvatarSenha.TabIndex = 29;
            this.picAvatarSenha.TabStop = false;
            // 
            // picAvatarLogin
            // 
            this.picAvatarLogin.Image = ((System.Drawing.Image)(resources.GetObject("picAvatarLogin.Image")));
            this.picAvatarLogin.Location = new System.Drawing.Point(29, 40);
            this.picAvatarLogin.Name = "picAvatarLogin";
            this.picAvatarLogin.Size = new System.Drawing.Size(32, 32);
            this.picAvatarLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picAvatarLogin.TabIndex = 28;
            this.picAvatarLogin.TabStop = false;
            // 
            // txtSenha
            // 
            this.txtSenha.Depth = 0;
            this.txtSenha.Hint = "";
            this.txtSenha.Location = new System.Drawing.Point(120, 97);
            this.txtSenha.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.PasswordChar = '*';
            this.txtSenha.SelectedText = "";
            this.txtSenha.SelectionLength = 0;
            this.txtSenha.SelectionStart = 0;
            this.txtSenha.Size = new System.Drawing.Size(343, 23);
            this.txtSenha.TabIndex = 12;
            this.txtSenha.UseSystemPasswordChar = false;
            this.txtSenha.Leave += new System.EventHandler(this.TxtSenha_Leave);
            // 
            // txtLogin
            // 
            this.txtLogin.Depth = 0;
            this.txtLogin.Hint = "";
            this.txtLogin.Location = new System.Drawing.Point(120, 49);
            this.txtLogin.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.PasswordChar = '\0';
            this.txtLogin.SelectedText = "";
            this.txtLogin.SelectionLength = 0;
            this.txtLogin.SelectionStart = 0;
            this.txtLogin.Size = new System.Drawing.Size(343, 23);
            this.txtLogin.TabIndex = 11;
            this.txtLogin.UseSystemPasswordChar = false;
            this.txtLogin.Leave += new System.EventHandler(this.TxtLogin_Leave);
            // 
            // lblsenha
            // 
            this.lblsenha.AutoSize = true;
            this.lblsenha.Depth = 0;
            this.lblsenha.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblsenha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblsenha.Location = new System.Drawing.Point(65, 97);
            this.lblsenha.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblsenha.Name = "lblsenha";
            this.lblsenha.Size = new System.Drawing.Size(50, 19);
            this.lblsenha.TabIndex = 17;
            this.lblsenha.Text = "Senha";
            // 
            // lbllogin
            // 
            this.lbllogin.AutoSize = true;
            this.lbllogin.Depth = 0;
            this.lbllogin.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbllogin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbllogin.Location = new System.Drawing.Point(65, 53);
            this.lbllogin.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbllogin.Name = "lbllogin";
            this.lbllogin.Size = new System.Drawing.Size(46, 19);
            this.lbllogin.TabIndex = 16;
            this.lbllogin.Text = "Login";
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.Color.Transparent;
            this.btnSair.FlatAppearance.BorderSize = 0;
            this.btnSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSair.ForeColor = System.Drawing.Color.YellowGreen;
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.Location = new System.Drawing.Point(306, 15);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(138, 36);
            this.btnSair.TabIndex = 26;
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.BtnSair_Click);
            // 
            // btnEntrar
            // 
            this.btnEntrar.BackColor = System.Drawing.Color.Transparent;
            this.btnEntrar.FlatAppearance.BorderSize = 0;
            this.btnEntrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEntrar.Image = ((System.Drawing.Image)(resources.GetObject("btnEntrar.Image")));
            this.btnEntrar.Location = new System.Drawing.Point(144, 15);
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(138, 39);
            this.btnEntrar.TabIndex = 27;
            this.btnEntrar.UseVisualStyleBackColor = false;
            this.btnEntrar.Click += new System.EventHandler(this.BtnEntrar_Click);
            // 
            // lblentrar
            // 
            this.lblentrar.AutoSize = true;
            this.lblentrar.Depth = 0;
            this.lblentrar.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblentrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblentrar.Location = new System.Drawing.Point(191, 57);
            this.lblentrar.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblentrar.Name = "lblentrar";
            this.lblentrar.Size = new System.Drawing.Size(49, 19);
            this.lblentrar.TabIndex = 28;
            this.lblentrar.Text = "Entrar";
            // 
            // lblsair
            // 
            this.lblsair.AutoSize = true;
            this.lblsair.Depth = 0;
            this.lblsair.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblsair.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblsair.Location = new System.Drawing.Point(356, 57);
            this.lblsair.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblsair.Name = "lblsair";
            this.lblsair.Size = new System.Drawing.Size(35, 19);
            this.lblsair.TabIndex = 29;
            this.lblsair.Text = "Sair";
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.YellowGreen;
            this.panel.Controls.Add(this.lblsair);
            this.panel.Controls.Add(this.lblentrar);
            this.panel.Controls.Add(this.btnEntrar);
            this.panel.Controls.Add(this.btnSair);
            this.panel.Location = new System.Drawing.Point(44, 486);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(581, 90);
            this.panel.TabIndex = 14;
            // 
            // errLogin
            // 
            this.errLogin.ContainerControl = this;
            this.errLogin.Icon = ((System.Drawing.Icon)(resources.GetObject("errLogin.Icon")));
            // 
            // errSenha
            // 
            this.errSenha.ContainerControl = this;
            this.errSenha.Icon = ((System.Drawing.Icon)(resources.GetObject("errSenha.Icon")));
            // 
            // TelaLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.YellowGreen;
            this.ClientSize = new System.Drawing.Size(664, 582);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.pnlTelaLogin);
            this.Controls.Add(this.panel1);
            this.MinimizeBox = false;
            this.Name = "TelaLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TelaLogin";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picAvatarUsuario)).EndInit();
            this.pnlTelaLogin.ResumeLayout(false);
            this.grpEfetuarLogin.ResumeLayout(false);
            this.grpEfetuarLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAvatarSenha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAvatarLogin)).EndInit();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errSenha)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox picAvatarUsuario;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlTelaLogin;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnEntrar;
        private MaterialSkin.Controls.MaterialLabel lblentrar;
        private MaterialSkin.Controls.MaterialLabel lblsair;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.GroupBox grpEfetuarLogin;
        private System.Windows.Forms.ComboBox cboAcessor;
        private MaterialSkin.Controls.MaterialLabel lblnivelA;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picAvatarSenha;
        private System.Windows.Forms.PictureBox picAvatarLogin;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSenha;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtLogin;
        private MaterialSkin.Controls.MaterialLabel lblsenha;
        private MaterialSkin.Controls.MaterialLabel lbllogin;
        private System.Windows.Forms.ErrorProvider errLogin;
        private System.Windows.Forms.ErrorProvider errSenha;
    }
}