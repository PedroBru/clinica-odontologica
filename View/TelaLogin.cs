﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using System.Data.SqlClient;

namespace View
{
    public partial class TelaLogin : MaterialSkin.Controls.MaterialForm
    {
        static TelaLogin objeto;

        public static string conectado;
        public static string acessor;

        public static TelaLogin Instance
        {
            get
            {
                if (objeto == null)
                {
                    objeto = new TelaLogin();
                }
                return objeto;
            }
        }

        public Panel PnlLogin
        {
            get { return pnlTelaLogin; }
            set { pnlTelaLogin = value; }
        }

        public TelaLogin()
        {
            InitializeComponent();
            //Criando um material theme manager e adicionando o formulário
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            //Definindo um esquema de Cor para formulário com tom Azul
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Green700, Primary.Green700,
                Primary.Green600, Accent.Amber700,
                TextShade.WHITE
         );

        }
        //Metodo para fechar a aplicação
        private void BtnSair_Click(object sender, EventArgs e)
        {

            Close();
            
        } 
        //Metodo para entra na tela principal
        private void BtnEntrar_Click(object sender, EventArgs e)
        {
            
            // ir em properties e conectar o banco
            SqlConnection con = new SqlConnection(Properties.Settings.Default.DBClinica_Bom_Jesus);

            SqlCommand cmd = new SqlCommand("SELECT * FROM conta WHERE loginconta =@loginconta " +
                                            " and senha=@senha and nivelA=@nivelA", con);

            cmd.Parameters.Add("@loginconta", SqlDbType.VarChar).Value = txtLogin.Text;
            cmd.Parameters.Add("@senha", SqlDbType.VarChar).Value = txtSenha.Text;
            cmd.Parameters.Add("nivelA", SqlDbType.VarChar).Value = cboAcessor.Text;

            // abrir a conexão
            con.Open();

            SqlDataReader abrir = null;

            abrir = cmd.ExecuteReader();

            if (abrir.Read())
            {
                conectado = txtLogin.Text;
                acessor = cboAcessor.Text;

                new TelaPrincipal
                {
                    Tela = this
                }.Show();
                this.Hide();

                // Metodo para limpa os campos 
                txtLogin.Text = ("");
                txtSenha.Text = ("");
                cboAcessor.SelectedIndex = -1;


                // Metodo para limpa os errorprovider
                errLogin.Clear();
                errSenha.Clear();

            }
            else
            {
                MessageBox.Show("Erro no email ou no ou na senha");
            }
        }
        //Metodo para não deixa o usuario deixa o campo vazio no textbox login
        private void TxtLogin_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLogin.Text))
            {
                errLogin.Icon = Properties.Resources.Erroicon;
                errLogin.SetError(txtLogin, "Erro adiciona seu Email ");
            }
            else
            {
                errLogin.Icon = Properties.Resources.Okicon;
                errLogin.SetError(txtLogin, "Ok");
            }
        }
        //Metodo para não deixa o usuario deixa o campo vazio no textbox 
        private void TxtSenha_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSenha.Text))
            {
                errSenha.Icon = Properties.Resources.Erroicon;
                errSenha.SetError(txtSenha, "Erro adiciona uma senha");
            }
            else
            {
                errSenha.Icon = Properties.Resources.Okicon;
                errSenha.SetError(txtLogin, "Ok");
            }
        }
    }
}
