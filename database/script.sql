    CREATE DATABASE DBClinica_Bom_Jesus;

	USE DBClinica_Bom_Jesus;

	CREATE TABLE conta(
			 id                    INT  PRIMARY KEY IDENTITY (1,1)   NOT NULL,
			 nome                  VARCHAR (80),                     
			 loginconta            VARCHAR (20)                      NOT NULL,
			 senha                 VARCHAR (10)                      NOT NULL, 
			 datanascimento        VARCHAR (20),                      
			 telefone              VARCHAR (20),                      
			 email                 VARCHAR (100),                     
			 endereco              VARCHAR (100),                     
			 numerocasa            VARCHAR (20),                      
			 complemento           VARCHAR (20),                      
			 cidade                VARCHAR (50),                      
			 bairro                VARCHAR (50),                      
			 cpf                   VARCHAR (20),                     
			 sexo                  VARCHAR (20),
			 nivelA                VARCHAR (50)
	);

	CREATE TABLE  paciente(
			 id                    INT PRIMARY KEY IDENTITY (1,1)    NOT NULL,
			 nome                  VARCHAR (100)                     NOT NULL,
			 datanascimento        DATE	                             NOT NULL,
			 telefone              VARCHAR (20)                      NOT NULL,
			 endereco              VARCHAR (100)                     NOT NULL,
			 numerocasa            VARCHAR (20)                      NOT NULL,
			 complemento           VARCHAR (20)                      NOT NULL,
			 cidade                VARCHAR (50)                      NOT NULL,
			 bairro                VARCHAR (50)                      NOT NULL,
			 cpf                   VARCHAR (20)                      NOT NULL   UNIQUE,
			 sexo                  VARCHAR (20)                      NOT NULL,
			 email                 VARCHAR (100)                  
	);

	CREATE TABLE servico(
			 id                    INT PRIMARY KEY IDENTITY (1,1)    NOT NULL,
			 nome                  VARCHAR (100)                     NOT NULL,
			 descricaodoservico    VARCHAR (500)                     NOT NULL,
			 custo                 DECIMAL,
	);

	CREATE TABLE consulta(
			 id                    INT PRIMARY KEY IDENTITY (1,1)    NOT NULL,
			 CONSTRAINT fk_UsuConsulta  FOREIGN KEY (idMedico)       REFERENCES conta    (id),
			 CONSTRAINT fk_PacConsulta  FOREIGN KEY (idPaciente)     REFERENCES Paciente (id),
			 CONSTRAINT fk_SerConsulta  FOREIGN KEY (idServico)      REFERENCES Servico  (id),
			 dataconsulta          DATE                              NOT NULL,
			 hora                  TIME                              NOT NULL,
			 idMedico              INT                               NOT NULL,
			 idPaciente            INT                               NOT NULL,
			 idServico             INT                               NOT NULL,
	);

	CREATE TABLE exame(
			 id                    INT PRIMARY KEY IDENTITY (1,1)    NOT NULL,
			 CONSTRAINT fk_ConsulExame FOREIGN KEY (idConsulta)      REFERENCES Consulta (id),
			 descricaodoexame      VARCHAR (500)                     NOT NULL,    
			 idConsulta            INT                               NOT NULL,
			 
     
	);
